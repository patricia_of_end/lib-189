const Cloud189 = require("./index").Cloud189Drive;
const axios = require("axios").default.create({
  headers: {
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 YaBrowser/17.1.0.2034 Safari/537.36",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",

  },
  maxRedirects: 0,
  maxContentLength: Number.MAX_SAFE_INTEGER
});
const setCookieParser = require("set-cookie-parser");
const log = require("./log_webhook").log;
const util = require("util")

/**
 * @returns {Promise<{ok:Boolean,msg:String,data:{open_e_189_url:String}}>}
 * @param {String} cookie_login_user 
 */
function createFromInitLoginUserCookie(cookie_login_user) {
  return new Promise(resolve => {
    axios.get('https://cloud.189.cn/udb/udb_login.jsp?pageId=1&redirectURL=/main.action', {
      headers: {
        cookie: 'COOKIE_LOGIN_USER=' + cookie_login_user
      },
      validateStatus: s => s == 302
    }).then(axresp => {
      // debugger
      if (axresp.headers['location']
        && axresp.headers['location'].includes("open.e.189.cn/api/logbox/oauth2/unifyAccountLogin.do")) {
        return resolve({
          ok: true,
          msg: "ok",
          data: {
            open_e_189_url: axresp.headers.location
          }
        })
      }
      log(`createFromInitLoginUserCookie LOCATION 不合预期`, `
      \n cookie_login_user=${cookie_login_user}
      \n return axresp.headers=${
        util.inspect(axresp.headers)
        }
      \n return axresp.data=${
        util.inspect(axresp.data)
        }
      `)
    }).catch(axerr => {
      log(`createFromInitLoginUserCookie axerr`, `
      \n cookie_login_user=${cookie_login_user}
      \n axerr=>
      \n ${util.inspect(axerr)}
      `)
    })
  })
}

/**
 * @returns {Promise<{ok:Boolean,msg:String,data:{setCookies:Array<{name:String,value:String}>}}>}
 * @param {string} link
 * @param {String} cookie_login_user 
 */
function visitOpenE189_UnifyAcountLogin(cookie_login_user, link) {
  return new Promise(resolve => {
    axios.get(link, {
      headers: {
        cookie: 'COOKIE_LOGIN_USER=' + cookie_login_user
      },
      validateStatus: s => s == 200 || s == 302
    }).then(axresp => {
      log('visitOpenE189_UnifyAcountLogin 200 or 302', `
      \n cookie_login_user = ${cookie_login_user}
      \n link = ${link}
      \n return axresp.status = ${axresp.status}
      \n return headers = ${util.inspect(axresp.headers)}
      \n return data = \n----\n${util.inspect(axresp.data)}
      `)
    }).catch(axerr => {
      log('visitOpenE189_UnifyAcountLogin AXERR', `visitOpenE189_UnifyAcountLogin AXERR`, `
      \n cookie_login_user = ${cookie_login_user}
      \n link = ${link}
      \n axerr = \n ${
        util.inspect(axerr)
        }
      `)
    })
  })
}


class NewCloud {
  /**
   * @param {String} init_SSON
   * @param {String} init_cookie_login_user 
   */
  constructor(init_cookie_login_user, init_SSON) {
    this.init_cookie_login_user = "undefined"||init_cookie_login_user;
    this.init_SSON = init_SSON;
    this.cookies = [{
      name: "COOKIE_LOGIN_USER",
      value: this.init_cookie_login_user
    }, {
      name: "SSON",
      value: init_SSON
    }]

    let boot = (async () => {
      // let visit189 = await this.visitcloud189mainaction();
      // await log("visit189成功了", `现在的cookie是:
      // \n ${this.cookies_as_header}`);
      let o_udb_login = await this.udb_login();
      await log("udb_login()成功", `现在的cookie是:
      \n ${this.cookies_as_header}
      \n unifyAccountLogin.do is
      \n${o_udb_login.next_link}`);
      let o_unify = await this.unifyAccountLogin(o_udb_login.next_link);
      await log("unifyAccountLogin()成功", `现在的cookie是:
      \n ${this.cookies_as_header}
      \n next link is
      \n${o_unify.next_link}`);
      let o_callback = await this.callbackUnify(o_unify.next_link);
      await log("o_callback()成功", `现在的cookie是:
      \n ${this.cookies_as_header}
      \n new USER_LOGIN is
      \n${o_callback.new_LOGIN_USER}`);
      let c189 = new Cloud189(o_callback.new_LOGIN_USER);
      let o_info = await c189.initInfoPromise;
      await log("await c189.initInfoPromise", `
      \n c189 现在的cookie是:
      \n ${c189.cookies_as_header}
      \n o_info is
      \n${JSON.stringify(o_info)}`);
    })();
  }

  get cookies_as_header() {
    return this.cookies.filter(e => e.value !== "").map(e => `${e.name}=${e.value}`).join("; ")
  }

  ___dealWithSetCookieHeaders(set_header) {
    let parsed = setCookieParser.parse(set_header);
    for (let p of parsed) {
      let find = this.cookies.findIndex(e => e.name == p.name);
      if (find >= 0) {
        this.cookies[find].value = p.value;
      } else {
        this.cookies.push({
          name: p.name,
          value: p.value
        })
      }
    }
  }


  visitcloud189mainaction() {
    let beforeCookie = this.cookies_as_header;
    return new Promise(resolve => {
      axios.get('https://cloud.189.cn/main.action', {
        headers: {
          cookie: this.cookies_as_header
        },
        validateStatus: s => s == 302
      }).then(axresp => {
        if (axresp.headers['set-cookie']) {
          this.___dealWithSetCookieHeaders(axresp.headers['set-cookie'])
          return resolve();
        }
        log(`visitcloud189mainaction() 没有setCookie`,
          `visitcloud189mainaction() 没有setCookie
        \n axresp.headers=${util.inspect(axresp.headers)}
        `)
      }).catch(axerr => {
        log(`visitcloud189mainaction() 不是302或者其他错误`,
          `visitcloud189mainaction() 不是302或者其他错误
        \n ${util.inspect(axerr)}
        `)
      })
    })
  }

  /**
   * @returns {Promise<{next_link:String}>}
   */
  udb_login() {
    let beforeCookie = this.cookies_as_header;
    return new Promise(resolve => {
      axios.get('https://cloud.189.cn/udb/udb_login.jsp?pageId=1&redirectURL=/main.action', {
        headers: {
          cookie: this.cookies_as_header
        },
        validateStatus: s => s == 302
      }).then(axresp => {
        if (axresp.headers['location']
          && axresp.headers['location'].includes("open.e.189.cn/api/logbox/oauth2/unifyAccountLogin.do")) {
          return resolve({
            next_link: axresp.headers['location']
          });
        }
        log(`udb_login() headers不对劲!!`,
          `
        \n axresp.headers= 
        \n\`\`\`
        ${JSON.stringify(axresp.headers)}
        \`\`\`
        \n axresp.data= \n
        ${util.inspect(axresp.data)}
        `);
      }).catch(axerr => {
        log(`udb_login() 不是302或者其他错误`,
          `
        \n ${util.inspect(axerr)}
        `)
      })
    })
  }

  /**
   * @returns {Promise<{next_link:String}>}
   * @param {String} unify_account_login_link 
   */
  unifyAccountLogin(unify_account_login_link) {
    let beforeCookie = this.cookies_as_header;
    return new Promise(resolve => {
      axios.get(unify_account_login_link, {
        headers: {
          cookie: this.cookies_as_header,
          "Referer": "https://cloud.189.cn/"
        },
        validateStatus: s => s == 302
      }).then(axresp => {
        this.___dealWithSetCookieHeaders(axresp.headers['set-cookie']);
        if (axresp.headers['location']
          && axresp.headers['location'].includes("cloud.189.cn/callbackUnify.action")) {
          return resolve({
            next_link: axresp.headers['location']
          })
        }
        log(` unifyAccountLogin() headers不对劲!!`,
          `\n beforeCookie = ${beforeCookie}
      \n axresp.headers= 
      \n\`\`\`
      ${JSON.stringify(axresp.headers)}
      \`\`\`
      \n axresp.data= \n
      ${util.inspect(axresp.data)}
      `);
      }).catch(axerr => {
        log(`unifyAccountLogin() 不是302或者其他错误`,
          `\n beforeCookie = ${beforeCookie}
        \n ${util.inspect(axerr)}
        `)
      })
    })
  }

  /**
   * @returns {Promise<{new_LOGIN_USER:String}>}
   * @param {String} callbackUnify_action_link 
   */
  callbackUnify(callbackUnify_action_link) {
    let beforeCookie = this.cookies_as_header;
    return new Promise(resolve => {
      axios.get(callbackUnify_action_link, {
        headers: {
          cookie: this.cookies_as_header
        },
        validateStatus: s => s == 200
      }).then(axresp => {
        if (axresp.headers['set-cookie']) {
          this.___dealWithSetCookieHeaders(axresp.headers['set-cookie']);
          log(`callbackUnify() 观察一下是啥html`,
            `\n
          \n${util.inspect(axresp.data)}
          \n
          `)
          return resolve({ new_LOGIN_USER: this.cookies.find(e => e.name
             == "COOKIE_LOGIN_USER").value })
        }
        log(`callbackUnify() 没有setCookie?`,
          `\n beforeCookie = ${beforeCookie}
        \n axresp.headers= 
        \n\`\`\`
        ${JSON.stringify(axresp.headers)}
        \`\`\`
        \n axresp.data= \n
        ${util.inspect(axresp.data)}
        \n 
        `)
      }).catch(axerr => {
        log(`callbackUnify() 不是200或者其他错误`,
          `\n beforeCookie = ${beforeCookie}
      \n ${util.inspect(axerr)}
      `)
      })
    })
  }
}



module.exports = {
  __debug: {
    createFromInitLoginUserCookie,
    visitOpenE189_UnifyAcountLogin,
    NewCloud
  }
}