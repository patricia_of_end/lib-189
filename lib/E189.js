const axios = require("axios").default.create({
  headers: {
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 YaBrowser/17.1.0.2034 Safari/537.36",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",

  },
  maxRedirects: 0,
  maxContentLength: Number.MAX_SAFE_INTEGER
});
const setCookieParser = require("set-cookie-parser");
const CaptchaMinJsFormatted = require("./captch.min.formatted");
const CommonAxerrHG = require("../tool/CommonAxerrHandler");
const JSDOM = require("jsdom").JSDOM;
const qs = require("querystring");
const SSON_189 = require("./SSON_189").SSON_189;

class E189 {
  /**
   * 
   * @param {String} phonenumber 
   * @param {String} raw_password 
   */
  constructor(phonenumber, raw_password) {
    this.phonenumber = phonenumber;
    this.raw_password = raw_password;
    this.cookies = [{
      name: "apm_pr",
      value: "0"
    }];
    this.get_sson_promise = this.getSsonInstance();
  }

  get cookies_as_header() {
    return this.cookies.filter(e => e.value !== "").map(e => `${e.name}=${e.value}`).join("; ")
  }

  ___dealWithSetCookieHeaders(set_header) {
    let parsed = setCookieParser.parse(set_header);
    for (let p of parsed) {
      let find = this.cookies.findIndex(e => e.name == p.name);
      if (find >= 0) {
        this.cookies[find].value = p.value;
      } else {
        this.cookies.push({
          name: p.name,
          value: p.value
        })
      }
    }
  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
   * oauth_unify_login_link:String
   * }}>}
   */
  step1_visitE189CN() {
    return new Promise(resolve => {
      axios.get("https://e.189.cn/index.do", {
        headers: {
          // cookie: this.cookies_as_header
        },
        validateStatus: s => s == 200 || s == 302
      }).then(axresp => {
        if (axresp.headers && axresp.headers['set-cookie']) {
          this.___dealWithSetCookieHeaders(axresp.headers['set-cookie'])
        }
        // debugger
        if (axresp.status == 200
          && axresp.headers
          && axresp.headers["content-type"]
          && axresp.headers["content-type"].includes("text/html")) {
          let dom = new JSDOM(axresp.data);
          let doc = dom.window.document;
          let iframe = doc.querySelector("iframe[src*=oauth]");
          if (iframe && iframe.getAttribute("src")
            && iframe.getAttribute("src").includes("open.e.189.cn/api/logbox/oauth2/unifyAccountLogin.do")) {
            return resolve({
              ok: true,
              msg: "ok",
              data: {
                oauth_unify_login_link: iframe.getAttribute("src")
              }
            })
          }

          throw {
            iframe: iframe,
            "iframe.src": iframe ? iframe.src : ""
          }
        }
        // debugger
        throw {
          "axresp.headers": axresp.headers,
          "axresp.data": axresp.data
        }
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      })
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,
   * data:{
   * lt:String,
   * reqId:String,
   * paramId:String,
   * captchaToken:String,
   * rsaKey:String
   * }}>}
   * @param {String} unify_account_link 
   */
  old_step2_visitUnifyAccountLink(unify_account_link) {
    return new Promise(resolve => {
      axios.get(unify_account_link, {
        headers: {
          cookie: this.cookies_as_header,
          referer: "https://e.189.cn/index.do"
        },
        validateStatus: s => s == 200
      }).then(axresp => {
        /**
         * @returns {{ok:Boolean,msg:String,data:{
         * lt:String,
         * paramId:String,
         * reqId:String
         * }}}
         */
        let parseLtParamidReqId = function () {
          /**@type {string} */
          let d = axresp.data;
          let matchLt = d.match(/lt\s=\s"([0-9A-Z]+)";/);
          if (!(matchLt && matchLt.length == 2)) {
            return { ok: false, msg: `can't find lt in::${axresp.data}` }
          }
          let matchParamId = d.match(/paramId\s=\s"([0-9A-Z]+)";/);
          if (!(matchParamId && matchParamId.length == 2)) {
            return { ok: false, msg: `can't find paramId in::${axresp.data}` }
          }
          let matchReqid = d.match(/reqId\s=\s"([0-9a-zA-Z]+)";/);
          if (!(matchReqid && matchReqid.length)) {
            return { ok: false, msg: `can't find reqId in::${axresp.data}` }
          }
          return {
            ok: true,
            msg: "ok",
            data: {
              reqId: matchReqid[1],
              paramId: matchParamId[1],
              lt: matchLt[1]
            }
          }
        }
        // debugger
        if (axresp.headers["set-cookie"]) {
          this.___dealWithSetCookieHeaders(axresp.headers["set-cookie"]);
        }
        if (axresp.headers["content-type"]
          && axresp.headers["content-type"].includes("text/html")) {
          let dom = new JSDOM(axresp.data);
          let doc = dom.window.document;
          let j_rsaKey = doc.querySelector("#j_rsaKey");
          let j_captcha = doc.querySelector("#j-captcha input");
          if (j_captcha && j_rsaKey && j_captcha.getAttribute("value")
            && j_rsaKey.getAttribute("value")) {
            // debugger
            let rsaKey = j_rsaKey.getAttribute("value")
            let captcha = j_captcha.getAttribute("value")
            let o_p = parseLtParamidReqId()
            if (!o_p.ok) {
              return resolve({
                ok: false,
                msg: o_p.msg
              })
            }
            return resolve({
              ok: true,
              msg: "ok",
              data: {
                lt: o_p.data.lt,
                paramId: o_p.data.paramId,
                reqId: o_p.data.reqId,
                rsaKey: rsaKey,
                captchaToken: captcha
              }
            })
          }
          throw {
            j_captcha,
            j_rsaKey
          }
        }
        throw {
          "axresp.headers": axresp.headers,
          "axresp.data": axresp.data
        }
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      })
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,
    * data:{
      * lt:String,
      * reqId:String,
      * appId:String,
      * next_url:String
      * }}>}
   * @param {String} unify_account_link 
   */
  step2_visitUnifyAccountLinkGetSeparateLink(unify_account_link) {
    return new Promise(resolve => {
      axios.get(unify_account_link, {
        headers: {
          cookie: this.cookies_as_header,
          referer: "https://e.189.cn/"
        },
        validateStatus: s => s == 302
      }).then(axresp => {
        if (axresp.headers["set-cookie"]) {
          this.___dealWithSetCookieHeaders(axresp.headers["set-cookie"]);
        }
        let next_url = axresp.headers.location;
        let url = require("url");
        let params = new url.URLSearchParams((new url.URL(next_url)).search);//URLSearchParams在nodev8里好像需要从url模块导入才行
        let reqId = params.get("reqId");
        let lt = params.get("lt");
        let appId = params.get("appId");//这里好像固定是E_189
        if (reqId && lt && appId) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              next_url: next_url,
              lt,
              reqId,
              appId
            }
          })
        } else {
          throw ("reqId && lt && appId 中有一个缺失")
        }

        debugger
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      })
    })
  }

  /**
   * @returns {Promise<{
   * ok:Boolean,
   * msg:String,
   * data:{
   * paramId:String,
   * returnUrl:String
   * }
   * }>}
   * @param {{
      * lt:String,
      * reqId:String,
      * appId:String,
      * next_url:String
      * }} step2_data 
   */
  step3A_appConf(step2_data) {
    return new Promise(resolve => {
      axios.post("https://open.e.189.cn/api/logbox/oauth2/appConf.do", "version=2.0&appKey=E_189", {
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          'content-length': 24,
          cookie: this.cookies_as_header,
          origin: 'https://open.e.189.cn',
          referer: step2_data.next_url,
          reqid: step2_data.reqId,
          lt: step2_data.lt
        }
      }).then(axresp => {
        if (axresp.data.msg == "成功" && axresp.data.result == "0") {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              paramId: axresp.data.data.paramId,
              returnUrl: axresp.data.data.returnUrl
            }
          })
        }
        throw axresp.data;
        debugger
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      })
    })
  }

  /**
   * @returns {Promise<{
    * ok:Boolean,
    * msg:String,
    * data:{
    * pubkey:String
    * }
    * }>}
   * @param {{
      * lt:String,
      * reqId:String,
      * appId:String,
      * next_url:String
      * }} step2_data 
   */
  step3B_encryptConf(step2_data) {
    return new Promise(resolve => {
      axios.post('https://open.e.189.cn/api/logbox/config/encryptConf.do', 'appId=E_189', {
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          'content-length': 11,
          cookie: this.cookies_as_header,
          origin: 'https://open.e.189.cn',
          referer: step2_data.next_url,
        }
      }).then(axresp => {
        if (axresp.data
          && axresp.data.data
          && axresp.data.data.pubKey) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              pubkey: axresp.data.data.pubKey
            }
          })
        }
        throw axresp.data;
        debugger
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      })

    })
  }

  /**
   * @returns {Promise<{
   * ok:boolean,
   * msg:String,
   * data:{
   * encodeuuid:String,
   * uuid:String,
   * encryuuid:String
   * }
   * }>}
   * @param {{
  * lt:String,
  * reqId:String,
  * appId:String,
  * next_url:String
  * }} step2_data 
   */
  step3C_getuuid(step2_data) {
    return new Promise(resolve => {
      axios.post('https://open.e.189.cn/api/logbox/oauth2/getUUID.do', "appId=E_189", {
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          'content-length': 11,
          cookie: this.cookies_as_header,
          origin: 'https://open.e.189.cn',
          referer: step2_data.next_url,
          reqid: step2_data.reqId,
          lt: step2_data.lt
        }
      }).then(axresp => {
        if (axresp.data.result === 0 && axresp.data.encryuuid && axresp.data.uuid) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              encodeuuid: axresp.data.encodeuuid,
              encryuuid: axresp.data.encryuuid,
              uuid: axresp.data.uuid
            }
          })
        }
        throw axresp.data;
        debugger
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      })
    })
  }


  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
    * SSON_value:String}}>}
   * @param {String} rsaPhone 
   * @param {String} rsaPwd 
   * @param {String} paramId 
   * @param {String} reqId 
   * @param {String} lt ,
   * @param {String} returnUrl
   */
  step4_loginSumit(rsaPhone, rsaPwd, paramId, reqId, lt, returnUrl, next_url) {
    return new Promise(resolve => {
      axios.post(`https://open.e.189.cn/api/logbox/oauth2/loginSubmit.do`, qs.stringify({
        version: 'v2.0',
        apToken: '',
        appKey: 'E_189',
        accountType: '02',
        userName: rsaPhone,
        password: rsaPwd,
        captchaType: '',
        validateCode: '',
        smsValidateCode: '',
        captchaToken: '',
        returnUrl: returnUrl,
        mailSuffix: '',
        dynamicCheck: 'FALSE',
        clientType: 1,
        cb_SaveName: 0,
        isOauth2: false,
        state: '',
        paramId: paramId
      }), {
        headers: {
          cookie: this.cookies_as_header,
          "Content-Type": 'application/x-www-form-urlencoded; charset=UTF-8',
          referer: next_url,
          reqId: reqId,
          lt: lt
        }
      }).then(axresp => {
        if (axresp.headers && axresp.headers["set-cookie"]) {
          this.___dealWithSetCookieHeaders(axresp.headers["set-cookie"])
        }
        if (axresp.data.msg == "登录成功") {
          let SSONindex = this.cookies.findIndex(e => e.name == "SSON");
          if (SSONindex >= 0 && this.cookies[SSONindex].value) {
            return resolve({
              ok: true,
              msg: "ok",
              data: {
                SSON_value: this.cookies[SSONindex].value
              }
            })
          } else {
            throw {
              "this.cookies": this.cookies
            }
          }
          debugger
        }
        throw axresp.data
        // debugger
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      })
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
   * SSON_value:String}}>}
   * @param {{
   * lt:String,
   * reqId:String,
   * paramId:String,
   * captchaToken:String,
   * rsaKey:String
   * }} step2_params 
   * @param {string} referer
   */
  old_step3_loginSubmit(step2_params, referer) {
    return new Promise(resolve => {
      let JSE = require("./JSE").JSEncrypt;
      let encrypt = new JSE;
      encrypt.setPublicKey(step2_params.rsaKey)
      // debugger


      let rsaPass = `{RSA}${encrypt.encrypt(this.raw_password)}`;
      let postData = qs.stringify({
        appKey: "E_189",
        accountType: '02',
        userName: this.phonenumber,
        password: rsaPass,
        validateCode: '',
        captchaToken: step2_params.captchaToken,
        returnUrl: 'https://e.189.cn/loginMiddle.do?returnUrlMid=https://e.189.cn/user/index.do',
        mailSuffix: '',
        dynamicCheck: 'FALSE',
        clientType: 1,
        cb_SaveName: 1,
        isOauth2: false,
        state: '',
        paramId: step2_params.paramId
      })
      axios.post(`https://open.e.189.cn/api/logbox/oauth2/loginSubmit.do`, postData, {
        headers: {
          cookie: this.cookies_as_header,
          "Content-Type": 'application/x-www-form-urlencoded; charset=UTF-8',
          "X-Requested-With": 'XMLHttpRequest',
          Origin: 'https://open.e.189.cn',
          lt: step2_params.lt,
          REQID: step2_params.reqId,
          referer: referer
        },
        validateStatus: s => s == 200
      }).then(axresp => {
        // debugger
        if (axresp.headers && axresp.headers["set-cookie"]) {
          this.___dealWithSetCookieHeaders(axresp.headers["set-cookie"])
        }
        if (axresp.data && axresp.data.result === 0) {
          let SSONindex = this.cookies.findIndex(e => e.name == "SSON");
          if (SSONindex >= 0 && this.cookies[SSONindex].value) {
            return resolve({
              ok: true,
              msg: "ok",
              data: {
                SSON_value: this.cookies[SSONindex].value
              }
            })
          } else {
            throw {
              "this.cookies": this.cookies
            }
          }
        }
        throw axresp.data
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      })
    })
  }


  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
   * sson:SSON_189
   * }}>}
   */
  getSsonInstance() {
    return new Promise(async resolve => {
      let step1 = await this.step1_visitE189CN();
      if (!step1.ok) {
        return resolve({
          ok: false,
          msg: `STEP1 fail:${step1.msg}`
        })
      }
      let step2 = await this.step2_visitUnifyAccountLinkGetSeparateLink(step1.data.oauth_unify_login_link);
      if (!step2.ok) {
        return resolve({
          ok: false,
          msg: `STEP2 fail:${step2.msg}`
        })
      }
      // debugger
      let step3a = await this.step3A_appConf(step2.data);
      if (!step3a.ok) {
        return resolve({
          ok: false,
          msg: `STEP3a fail:${step3a.msg}`
        })
      }
      let step3b = await this.step3B_encryptConf(step2.data);
      let step3c = await this.step3C_getuuid(step2.data);
      if (!(step3b.ok && step3c.ok)) {
        return resolve({
          ok: false,
          msg: `STEP3b or 3c fail:${step3b.msg}+${step3c.msg}`
        })
      }
      let jse = new CaptchaMinJsFormatted.JSEncrypt;
      jse.setKey(step3b.data.pubkey);
      let phone_rsaed = `{NRP}${jse.encrypt(this.phonenumber)}`;
      let password_rsaed = `{NRP}${jse.encrypt(this.raw_password)}`;
      console.log(step3a.data.returnUrl)
      let step4 = await this.step4_loginSumit(phone_rsaed,
        password_rsaed, step3a.data.paramId, step2.data.reqId, step2.data.lt, step3a.data.returnUrl,
        step2.data.next_url)
      // debugger
      if (!step4.ok) {
        return resolve({
          ok: false,
          msg: `STEP4fail:${step4.msg}`
        })
      }
      let sson = new SSON_189(step4.data.SSON_value);
      resolve({
        ok: true,
        msg: "ok",
        data: {
          sson: sson
        }
      })
    })
  }
}


module.exports = {
  E189
}