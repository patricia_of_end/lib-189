const Cloud189 = require("./index").Cloud189Drive;
const axios = require("axios").default.create({
  headers: {
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 YaBrowser/17.1.0.2034 Safari/537.36",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",

  },
  maxRedirects: 0,
  maxContentLength: Number.MAX_SAFE_INTEGER
});
const setCookieParser = require("set-cookie-parser");
const CommonAxerrHG = require("../tool/CommonAxerrHandler");
class SSON_189 {
  /**
   * 
   * @param {String} valid_sson_value 
   */
  constructor(valid_sson_value) {
    this.cookies = [{
      name: "SSON",
      value: valid_sson_value
    }]
  }

  get cookies_as_header() {
    return this.cookies.filter(e => e.value !== "").map(e => `${e.name}=${e.value}`).join("; ")
  }

  ___dealWithSetCookieHeaders(set_header) {
    let parsed = setCookieParser.parse(set_header);
    for (let p of parsed) {
      let find = this.cookies.findIndex(e => e.name == p.name);
      if (find >= 0) {
        this.cookies[find].value = p.value;
      } else {
        this.cookies.push({
          name: p.name,
          value: p.value
        })
      }
    }
  }

  /**
   * @returns {Promise<{ok:Boolean,
   * msg:String,
   * data:{
   * c189:Cloud189}}>}
   */
  old_getNewCloud189Drive() {
    return new Promise(async resolve => {
      let o_step1 = await this.old_step1_udbLogin();
      if (!o_step1.ok) {
        return resolve({
          ok: false,
          msg: `STEP1 fail:${o_step1.msg}`
        })
      }
      let o_step2 = await this.old_step2_unifyAccountLogin(o_step1.data.oauth_unify_link);
      if (!o_step2.ok) {
        return resolve({
          ok: false,
          msg: `STEP2 fail:${o_step2.msg}`
        })
      }
      let o_step3 = await this.old_step3_callbackUnify(o_step2.data.callback_unify_link);
      if (!o_step3.ok) {
        return resolve({
          ok: false,
          msg: `STEP3 fail:${o_step3.msg}`
        })
      }
      let c189 = new Cloud189(o_step3.data.COOKIE_LOGIN_USER);
      resolve({
        ok: true,
        msg: "ok",
        data: {
          c189: c189
        }
      })
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,
    * msg:String,
    * data:{
    * c189:Cloud189}}>}
    */
  getNewCloud189Drive() {
    return new Promise(async resolve => {
      let o_step1 = await this.step1_loginUrlaction();
      // debugger
      if (!o_step1.ok) {
        return resolve({
          ok: false,
          msg: `STEP1 fail:${o_step1.msg}`
        })
      }
      let o_step2 = await this.step2_unifyAccountLogin(o_step1.data.oauth_unify_link);
      if (!o_step2.ok) {
        return resolve({
          ok: false,
          msg: `STEP2 fail:${o_step2.msg}`
        })
      }
      let o_step3 = await this.step3_callbackUnify(o_step2.data.callback_unify_link);
      if (!o_step3.ok) {
        return resolve({
          ok: false,
          msg: `STEP3 fail:${o_step3.msg}`
        })
      }
      let c189 = new Cloud189(o_step3.data.COOKIE_LOGIN_USER);
      resolve({
        ok: true,
        msg: "ok",
        data: {
          c189: c189
        }
      })
    })
  }

  /**
 * @returns {Promise<{ok:Boolean,msg:String,data:{
  * oauth_unify_link:String
  * }}>}
  */
  step1_loginUrlaction() {
    return new Promise(resolve => {
      axios.get("https://cloud.189.cn/api/portal/loginUrl.action?redirectURL=https%3A%2F%2Fcloud.189.cn%2Fweb%2Fredirect.html", {
        headers: {
          cookies: this.cookies_as_header,
          referer: "https://cloud.189.cn/"
        },
        validateStatus: s => s == 302 || s == 301 || s == 200
      }).then(axresp => {
        let ccs = this.cookies_as_header;
        // debugger
        if (axresp.headers['location']
          && axresp.headers['location'].includes("open.e.189.cn/api/logbox/oauth2/unifyAccountLogin.do")) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              oauth_unify_link: axresp.headers['location']
            }
          })
        }
        throw axresp.headers;
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      });
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
   * oauth_unify_link:String
   * }}>}
   */
  old_step1_udbLogin() {
    return new Promise(resolve => {
      axios.get("https://cloud.189.cn/udb/udb_login.jsp?pageId=1&redirectURL=/main.action", {
        headers: {
          cookies: this.cookies_as_header,
          referer: "https://cloud.189.cn/"
        },
        validateStatus: s => s == 302 || s == 301
      }).then(axresp => {
        let ccs = this.cookies_as_header;
        // debugger
        if (axresp.headers['location']
          && axresp.headers['location'].includes("open.e.189.cn/api/logbox/oauth2/unifyAccountLogin.do")) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              oauth_unify_link: axresp.headers['location']
            }
          })
        }
        throw axresp.headers;
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      });
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
   * callback_unify_link:String
   * }}>}
   * @param {String} unify_account_login_link 
   */
  old_step2_unifyAccountLogin(unify_account_login_link) {
    return new Promise(resolve => {
      axios.get(unify_account_login_link, {
        headers: {
          cookie: this.cookies_as_header,
          "Referer": "https://cloud.189.cn/"
        },
        validateStatus: s => s == 302
      }).then(axresp => {
        this.___dealWithSetCookieHeaders(axresp.headers['set-cookie']);
        if (axresp.headers['location']
          && axresp.headers['location'].includes("cloud.189.cn/callbackUnify.action")) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              callback_unify_link: axresp.headers['location']
            }
          })
        }
        throw axresp.headers;
      }).catch(axerr => {
        return CommonAxerrHG(resolve)(axerr);
      })
    })
  }

  /**
 * @returns {Promise<{ok:Boolean,msg:String,data:{
  * callback_unify_link:String
  * }}>}
  * @param {String} unify_account_login_link 
  */
  step2_unifyAccountLogin(unify_account_login_link) {
    return new Promise(resolve => {
      axios.get(unify_account_login_link, {
        headers: {
          cookie: this.cookies_as_header,
          "Referer": "https://cloud.189.cn/"
        },
        validateStatus: s => s == 302
      }).then(axresp => {
        this.___dealWithSetCookieHeaders(axresp.headers['set-cookie']);
        if (axresp.headers['location']
          && axresp.headers['location'].includes("cloud.189.cn/api/portal/callbackUnify.action")) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              callback_unify_link: axresp.headers['location']
            }
          })
        }
        throw axresp.headers;
      }).catch(axerr => {
        return CommonAxerrHG(resolve)(axerr);
      })
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,
   * msg:String,
   * data:{
   * COOKIE_LOGIN_USER:String
   * }}>}
   * @param {String} callbackUnify_action_link 
   */
  old_step3_callbackUnify(callbackUnify_action_link) {
    return new Promise(resolve => {
      axios.get(callbackUnify_action_link, {
        headers: {
          cookie: this.cookies_as_header,
          "Referer": "https://cloud.189.cn/"
        },
        validateStatus: s => s == 200
      }).then(axresp => {
        if (axresp.headers['set-cookie']) {
          this.___dealWithSetCookieHeaders(axresp.headers['set-cookie']);
        }
        let LOGIN_USER_index = this.cookies.findIndex(e => e.name == "COOKIE_LOGIN_USER");
        if (LOGIN_USER_index >= 0 && this.cookies[LOGIN_USER_index].value.length) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              COOKIE_LOGIN_USER: this.cookies[LOGIN_USER_index].value
            }
          })
        }
        throw {
          "axresp.headers": axresp.headers,
          "axresp.data": axresp.data,
          "this.cookies": this.cookies
        }
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr)
      })
    })
  }

  /**
 * @returns {Promise<{ok:Boolean,
  * msg:String,
  * data:{
  * COOKIE_LOGIN_USER:String
  * }}>}
  * @param {String} callbackUnify_action_link 
  */
  step3_callbackUnify(callbackUnify_action_link) {
    return new Promise(resolve => {
      axios.get(callbackUnify_action_link, {
        headers: {
          cookie: this.cookies_as_header,
          "Referer": "https://cloud.189.cn/"
        },
        validateStatus: s => s == 302
      }).then(axresp => {
        if (axresp.headers['set-cookie']) {
          this.___dealWithSetCookieHeaders(axresp.headers['set-cookie']);
        }
        let LOGIN_USER_index = this.cookies.findIndex(e => e.name == "COOKIE_LOGIN_USER");
        if (LOGIN_USER_index >= 0 && this.cookies[LOGIN_USER_index].value.length) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              COOKIE_LOGIN_USER: this.cookies[LOGIN_USER_index].value
            }
          })
        }
        throw {
          "axresp.headers": axresp.headers,
          "axresp.data": axresp.data,
          "this.cookies": this.cookies
        }
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr)
      })
    })
  }

}

module.exports = {
  SSON_189
}