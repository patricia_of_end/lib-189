const CommonAxerrHG = require("../tool/CommonAxerrHandler")
const setCookieParser = require("set-cookie-parser");
const url = require("url");
const querystring = require("querystring");
const rs = require("randomstring");
const buffer = require("buffer");
const rn = require("random-number");
const FormData = require("form-data");
const path = require("path");
const Toolbox = require("../tool/toolbox");
const fs = require("fs");
const runParallel = require("run-parallel-limit");
const util = require("util");
const IllegalWords = require("../config/IllegalWords");
const filesize = require("filesize");
const upload_endpoint = require("./upload_endpoint");
const EventEmitter = require("events").EventEmitter;
const PromiseQueue = require("promise-queue");

let _axios_instance = require("axios").default.create({
  headers: {
    "connection": "upgrade",
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "accept-language": "en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3",
    "upgrade-insecure-requests": "1",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18362",
    "accept-encoding": "gzip, deflate",
    "X-Requested-With": 'XMLHttpRequest',
    Referer: 'https://cloud.189.cn/main.action'
  },
  validateStatus: s => s == 200,
  maxRedirects: 0,
  maxContentLength: Number.MAX_SAFE_INTEGER
});

class Cloud189Drive {
  /**
   * 
   * @param {String} login_user_cookie 
   */
  constructor(login_user_cookie) {
    this.login_user_cookie = login_user_cookie;
    this.append_cookies = [{
      name: "apm_pr",
      value: "0"
    }];

    this.initInfoPromise = this.getLoginedInfos();
    this.upload_queue = new PromiseQueue(5, Infinity);

  }

  get cookies_as_header() {
    return `COOKIE_LOGIN_USER=${this.login_user_cookie}; ${this.append_cookies.map(e => `${e.name}=${e.value}`).join("; ")}`
  }

  get axios() {
    return _axios_instance;
  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,
   * data:{
   * superVip:Number,
   * superBeginTime:String,
   * leftTimeMs:Number,
   * uploadUrl:String,
   * superEndTime:String,
   * orderChannel:String,
   * isSpg:Boolean
   * }}>}
   * @todo 
   */
  getUserUploadUrl() {

  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{userId:Number,userAccount:String,
   * sessionKey:String,
   * quota:Number,
   * usedSize:Number,
   * used189Size:Number,
   * domainName:String,
   * nickname:String}}>}
   * @description 获取这个账号相关的信息,包括重要的sessionKey
   */
  getLoginedInfos() {
    return new Promise(resolve => {
      this.axios.get('https://cloud.189.cn/v2/getLoginedInfos.action', {
        params: {
          showPC: true,
          noCache: Math.random()
        },
        headers: {
          cookie: this.cookies_as_header
        }
      }).then(axresp => {
        // debugger
        if (axresp.headers["set-cookie"] && axresp.headers["set-cookie"].length) {
          let parsed = setCookieParser(axresp.headers["set-cookie"]);
          for (let p of parsed) {
            this.append_cookies.push({
              name: p.name,
              value: p.value
            })
          }
        }
        if (axresp.data && axresp.data['sessionKey']
          && axresp.data.quota) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              quota: axresp.data.quota,
              used189Size: axresp.data.used189Size,
              usedSize: axresp.data.usedSize,
              userAccount: axresp.data.userAccount,
              userId: axresp.data.userId,
              sessionKey: axresp.data.sessionKey,
              domainName: axresp.data.domainName,
              nickname: axresp.data.nickname
            }
          })
        }
        throw axresp.data;
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr);
      })
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,
   * data:{
   * pageNum:Number,
   * pageSize:Number,
   * recordCount:Number,
   * path:Array<{fileId:String,fileName:String}>,
   * data:Array<ModelData_inListFile>
   * }}>}
   * @param {Number} page_index 从1开始数起
   * @param {Number} folder_id 
   * @param {String} keyword 
   * @param {1|2|3} orderBy 1表示文件名,2表示大小,3表示时间(好像是修改时间?不是很确定)
   * @param {"ASC"|"DESC"} order
   */
  listFiles_old(folder_id = -11, keyword = "", page_index = 1, orderBy = 3, order = "DESC") {
    return new Promise(resolve => {
      this.axios.get('https://cloud.189.cn/v2/listFiles.action', {
        params: {
          fileId: folder_id,
          mediaType: "",
          keyword: keyword,
          inGroupSpace: false,
          orderBy: orderBy,
          order: order,
          pageNum: page_index,
          pageSize: 60,
          noCache: Math.random()
        },
        headers: {
          cookie: this.cookies_as_header
        }
      }).then(axresp => {
        if (axresp.data && axresp.data['path']
          && axresp.data['data']) {
          /**@type {Array<{fileId:String,fileName:String}>} */
          let ps = []
          for (let p of axresp.data['path']) {
            ps.push({
              fileId: p['fileId'],
              fileName: p['fileName']
            })
          }
          let items = [];
          for (let item of axresp.data['data']) {
            // debugger
            items.push(new ModelData_inListFile(item['fileId'],
              item.fileName, item.isFolder, item.parentId, item.createTime, item.lastOpTime,
              item.isFolder ? 0 : item['fileSize'],
              item.isFolder ? "" : item['downloadUrl']));
          }
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              pageNum: axresp.data.pageNum,
              pageSize: axresp.data.pageSize,
              recordCount: axresp.data.recordCount,
              path: ps,
              data: items
            }
          })
        }

        throw axresp.data;
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr)
      })
    })
  }

  /**
   * 
   * @param {Number} folder_id 
   * @param {Number} page_index 
   * @param {"lastOpTime"|"filesize"|"filename"} orderBy 
   * @param {Boolean} descending
   * @returns {Promise<{
   * ok:Boolean,
   * msg:String,
   * data:{
   * count:Number,
   * fileList:Array<{
   * id:String,
   * name:String,
   * size:Number,
   * lastOpTime:String
   * }>,
   * folderList:Array<{
   * id:String,
   * name:String,
   * lastOpTime:String,
   * parentId:Number,
   * fileCount:Number
   * }>
   * }
   * }>} 
   */
  listFiles_20220301(folder_id = -11, page_index = 1, orderBy = "lastOpTime", descending = true) {
    return new Promise(resolve => {
      this.axios.get('https://cloud.189.cn/api/open/file/listFiles.action', {
        params: {
          noCache: Math.random(),
          pageSize: 60,
          pageNum: page_index,
          mediaType: 0,
          folderId: folder_id,
          iconOption: 5,
          orderBy: orderBy,
          descending: descending,
        },
        headers: {
          cookie: this.cookies_as_header,
          accept: 'application/json;charset=UTF-8',
          'sign-type': 1,//我操 这个header能决定返回的id是数字还是String
          Referer: `https://cloud.189.cn/web/main/file/folder/${folder_id}`
        }
      }).then(axresp => {
        if (axresp.data['res_code'] === 0) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              count: axresp.data.fileListAO.count,
              fileList: axresp.data.fileListAO.fileList,
              folderList: axresp.data.fileListAO.folderList
            }
          })
          debugger
        }
        throw axresp.data;
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr)
      })
    })
  }

  /**
   * @returns {Promise<{
   * ok:Boolean,
   * msg:String,
   * data:{
   * oos_ctyun_link:String
   * }
   * }>}}
   * @param {Number} file_id 
   */
  getDownloadUrl_20220301(file_id) {
    //https://cloud.189.cn/api/open/file/getFileDownloadUrl.action?noCache=0.18265564056052797&fileId=8130126768051591
    return new Promise(async resolve => {
      let step1Func = () => new Promise(r1 => {
        this.axios.get('https://cloud.189.cn/api/open/file/getFileDownloadUrl.action', {
          params: {
            noCache: Math.random(),
            fileId: file_id
          },
          headers: {
            accept: 'application/json;charset=UTF-8',
            cookie: this.cookies_as_header,
            'sign-type': 1,
            Referer: `https://cloud.189.cn/web/main/file/folder/-11`
          }
        }).then(axresp => {
          if (axresp.data['res_code'] === 0) {
            // debugger
            return r1({
              ok: true, msg: "ok",
              data: {
                link: axresp.data.fileDownloadUrl
              }
            })
          }
          throw axresp.data;
        }).catch(axerr => {
          CommonAxerrHG(r1)(axerr)
        })
      });
      let o_step1 = await step1Func();
      if (!o_step1.ok) {
        return resolve({ ok: false, msg: "getFileDownloadUrl.action:" + o_step1.msg })
      }
      this.axios.get(o_step1.data.link, {
        headers: {
          cookie: this.cookies_as_header
        },
        validateStatus: s => s == 302 || s == 301
      }).then(axresp => {
        if (axresp.headers.location) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              oos_ctyun_link: axresp.headers.location
            }
          })
        }
        throw axresp.data;
        debugger
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr)
      })
      // debugger
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
   * location:String}}>}
   * @param {String} _url 
   */
  visitGetLocation(_url) {
    return new Promise(resolve => {
      this.axios.get(_url, {
        headers: {
          cookie: this.cookies_as_header
        },
        maxRedirects: 0,
        validateStatus: s => s == 302
      }).then(axresp => {
        if (axresp.headers && axresp.headers['location']) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              location: axresp.headers['location']
            }
          })
        }
        throw {
          "axresp.headers": axresp.headers
        }
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr)
      })
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
   * ctyunapi_link:String
   * }}>}
   * @param {String} fileStr 比如https://cloud.189.cn/downloadFile.action?fileStr=6862D73137439996A347A89EFC97D7891C2BC5731E5794E2BFADEB66BAE64E1D18E44425D0D055E1915BB5255A861C538B1EC9EB66293E45&downloadType=1
   */
  getDirectDownload(fileStr) {
    return new Promise(async resolve => {
      let step1 = await this.visitGetLocation(`https://cloud.189.cn/downloadFile.action?fileStr=${
        fileStr
        }&downloadType=1`);
      if (!step1.ok) {
        return resolve({
          ok: false,
          msg: `get download STEP 1 fail:${fileStr}:${step1.msg}`
        })
      }
      let step2 = await this.visitGetLocation(step1.data.location);
      if (!step2.ok) {
        return resolve({
          ok: false,
          msg: `get download STEP 2 fail:${fileStr}:${step2.msg}`
        })
      }
      let parsed = url.parse(step2.data.location);
      if (parsed.host.includes("ctyunapi.cn")) {
        return resolve({
          ok: true,
          msg: "ok",
          data: {
            ctyunapi_link: step2.data.location
          }
        })
      }
      return resolve({
        ok: false,
        msg: `download STEP 2 get link is NOT ctyunapi.cn:${step2.data.location}`
      })
    })
  }

  /**
   * @description `fileSize`and`subFileCount` are available when is folder;downloadparam_fileStr when it is not folder
   * @returns {Promise<{ok:Boolean,msg:String,data:{
    * createTime_E8:String,
    * lastOpTime_E8:String,
    * fileName:String,
    * fileId:string,
    * fileSize:Number,
    * isFolder:Boolean,
    * parentId:String,
    * downloadparam_fileStr:String,
    * subFileCount:Number
    * }}>}
   * @param {String} fileId 
   */
  getFileInfo(fileId) {

    return new Promise(resolve => {
      if (fileId == "-11") {
        return resolve({
          ok: false,
          msg: `-11 root can not get info`
        })
      }
      this.axios.get(`https://cloud.189.cn/v2/getFileInfo.action`, {
        params: {
          fileId: fileId,
          noCache: Math.random()
        },
        headers: {
          cookie: this.cookies_as_header,
          Referer: `https://cloud.189.cn/main.action`
        }
      }).then(axresp => {
        if (axresp.data
          && axresp.data['createTime']
          && axresp.data['fileName']) {
          if (axresp.data.isFolder) {
            return resolve({
              ok: true,
              msg: "ok",
              data: {
                createTime_E8: axresp.data['createTime'],
                fileId: axresp.data['fileId'],
                fileName: axresp.data['fileName'],
                fileSize: 0,
                isFolder: true,
                lastOpTime_E8: axresp.data['lastOpTime'],
                parentId: axresp.data['parentId'],
                subFileCount: axresp.data['subFileCount'],
                downloadparam_fileStr: ""
              }
            })
          } else {
            let downloadUrl = axresp.data['downloadUrl'];
            let p1 = url.parse(downloadUrl);
            let p2 = querystring.parse(p1.query);
            if (!p2['fileStr']) {
              return resolve({
                ok: false,
                msg: `can't get fileStr from link \`${downloadUrl}\``
              })
            } else {
              return resolve({
                ok: true,
                msg: "ok",
                data: {
                  createTime_E8: axresp.data['createTime'],
                  fileId: axresp.data['fileId'],
                  fileName: axresp.data['fileName'],
                  fileSize: axresp.data['fileSize'],
                  isFolder: false,
                  lastOpTime_E8: axresp.data['lastOpTime'],
                  parentId: axresp.data['parentId'],
                  subFileCount: 0,
                  downloadparam_fileStr: p2['fileStr']
                }
              })
            }
          }
        }
        if (Object.keys(axresp.data).length == 0) {
          throw `return value is {}`
        }
        throw axresp.data;
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr)
      })
    })
  }

  /**
   * @description 要注意重名问题
   * @returns {Promise<{ok:Boolean,
   * msg:String,
   * data:{
   * fileId:String,
   * isNew:Boolean
   * }}>}
   * @param {String} parent_id 
   * @param {String} _folder_name 不能超过250个字符
   */
  createFolder(parent_id, _folder_name) {
    let folder_name = IllegalWords.ReplaceIllegalWords(_folder_name);
    console.log(_folder_name, "=>", folder_name)
    return new Promise(resolve => {
      this.axios.get('https://cloud.189.cn/v2/createFolder.action', {
        params: {
          parentId: parent_id,
          fileName: folder_name
        },
        headers: {
          cookie: this.cookies_as_header
        }
      }).then(axresp => {
        if (axresp.data && axresp.data['fileId']) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              fileId: axresp.data['fileId'],
              isNew: !!axresp.data['isNew']
            }
          })
        }
        throw axresp.data;
      }).catch(axerr => {
        CommonAxerrHG(resolve)(axerr)
      })
    })
  }

  /**
   * @returns {Promise<{ok:Boolean,
    * msg:String,
    * data:{
    * fileId:String,
    * }}>}
   * @param {String} parent_id 
   * @param {String} folder_name 不能超过250个字符
   */
  createFolderUtilIsNew(parent_id, folder_name) {
    return new Promise(async resolve => {
      let o_try = await this.createFolder(parent_id, folder_name);
      if (o_try.ok && o_try.data.isNew) {
        return resolve({
          ok: true,
          msg: "ok",
          data: { fileId: o_try.data.fileId }
        })
      }
      if (!o_try.ok) {
        return resolve({
          ok: false,
          msg: `can not create folder(${folder_name}):${o_try.msg}`
        })
      }
      let nextName = folder_name;
      if (folder_name.length <= 245) {
        nextName = `${folder_name}.${rs.generate(4)}`
      } else {
        nextName = folder_name.substr(0, 245);
        nextName = `${nextName}.${rs.generate(4)}`
      }
      let o_next = await this.createFolderUtilIsNew(parent_id, nextName);
      if (o_next.ok) {
        return resolve({
          ok: true,
          msg: "ok",
          data: {
            fileId: o_next.data.fileId
          }
        })
      }
      return resolve({
        ok: false,
        msg: `fail when this.createFolderUtilIsNew(parent_id, nextName=${nextName})::${o_next.msg}`
      })

    })
  }



  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
   * id:String
   * }}>}
   * @param {import("fs").ReadStream|string} stream 
   * @param {String} _filename 
   * @param {String} parent_id 
   */
  uploadFile__Hunan(stream, _filename, parent_id = "-11") {
    let filename = IllegalWords.ReplaceIllegalWords(_filename);
    // console.log(_filename, "->", filename)
    return new Promise(async resolve => {
      let o_ss = await this.initInfoPromise;
      if (!o_ss.ok) {
        return resolve({
          ok: false,
          msg: `can not get sessionKey:${o_ss.msg}`
        })
      }
      let form = new FormData;
      form.append("sessionKey", o_ss.data.sessionKey);
      form.append("parentId", parent_id);
      form.append("albumId", 'undefined');
      form.append("opertype", 1);
      form.append("fname", filename);
      //**@强行设置filename和contentType是很重要的 否则电信的那个傻逼服务器解析不出来 */
      form.append("Filedata", stream, {
        filename: filename,
        contentType: "application/octec-stream",
      });
      let upload_has_response = false;
      // let OneOrTwo = rn({ min: 1, max: 1, integer: true });
      // let good_provinces = [
      //   "chongq",
      //   "anh",
      //   "anh",
      //   "chongq",
      //   // "shangh",
      //   // "fuj",
      //   "shanx",
      //   "shanx",
      //   "xinj"
      //   // "hn02"
      // ]
      // let testProvinces = [
      //   "hun"
      // ]
      // good_provinces = testProvinces;
      let endPoint = upload_endpoint.getUploadEndpoint();
      console.log(Date(), endPoint, _filename, "start__");
      let parsed = url.parse(endPoint);
      /**
       * @type {number[]}
       */
      let upload_speedlogs = [];
      let get_upload_info = () => {
        let max = 0;
        let total = 0;
        let speed_history = "";
        for (let s of upload_speedlogs) {
          if (s > max) { max = s };
          total += s;
          speed_history += `${filesize(s)}/s `
        }
        let ave = total / upload_speedlogs.length;
        return `upload endpoint is ${endPoint}
        \nmax_speed is ${filesize(max)}/s,ave_speed is ${filesize(ave)}/s;
        \n speed log is :${speed_history}`

      }
      form.submit({
        host: parsed.host,
        path: parsed.path,
        headers: {
          Origin: 'https://cloud.189.cn',
          Referer: 'https://cloud.189.cn/main.action',
          "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 YaBrowser/17.1.0.2034 Safari/537.36"
        },
        protocol: parsed.protocol
      }, (err, response) => {
        upload_has_response = true;
        if (err) {
          let msg = `${endPoint},${filename},form.submit err::${err.message}`
          console.log(msg);
          return resolve({
            ok: false,
            msg: msg
          });
        }
        upload_has_response = true;

        response.on("error", (err) => {
          console.log(`response.on(error):${err.message}`);
          return resolve({
            ok: false,
            msg: `response.on(error): ${err.message}`
          });
        });
        if (!response.headers['content-type']) {
          console.log(`no response content - type`);
          return resolve({
            ok: false,
            msg: `no response content - type`
          });
        }
        if (!(response.headers['content-type'].startsWith("application/json"))) {
          let msg = `${_filename},${endPoint},response content - type is ${response.headers['content-type']},status is ${response.statusCode}`
          console.log(msg);

          return resolve({
            ok: false,
            msg: msg
          });
        }
        let buf = new buffer.Buffer("");
        response.on("data", chunk => buf += chunk);
        response.on("end", () => {
          let json_str = buf.toString();
          try {
            let j = JSON.parse(json_str);
            if (j.MD5
              && j.id
              && j.name) {
              console.log(Date(), endPoint, _filename, "response ok")
              return resolve({
                ok: true,
                msg: "ok",
                data: {
                  id: j.id
                }
              })
            }

            return resolve({
              ok: false,
              msg: json_str
            })
          } catch (e) {
            console.log("can't parse json", json_str)
            return resolve({
              ok: false,
              msg: `can't parse json::${json_str}`
            })
          }
        })
      });
      let bytesRead = 0;
      let interval_mark = 0;
      if (util.isNumber(stream.bytesRead)) {
        const MIN_SPEED = 1 * 700 * 1024;
        const CHECK_SECONDS = 60;
        interval_mark = setInterval(() => {
          let br = stream.bytesRead;
          let speed = filesize(br - bytesRead) + "/s";
          console.log(_filename, speed);
          upload_speedlogs.push(br - bytesRead);
          bytesRead = br;

          if (upload_has_response) {
            console.log(" clearInterval(interval_mark);")
            clearInterval(interval_mark);
            return;

          }


          if (upload_speedlogs.length >= CHECK_SECONDS) {
            let last60sec = upload_speedlogs.slice(-CHECK_SECONDS);
            if (last60sec.filter(e => e >= MIN_SPEED).length == 0) {
              let nowave = upload_speedlogs.reduce((a, b) => a + b) / upload_speedlogs.length;
              if (nowave <= MIN_SPEED * 1.2) {
                let errmsg = "speed err,too slow speed::" + get_upload_info();
                // console.log(_filename, errmsg);
                stream.destroy(new Error(errmsg));//这个能在form.on("error")触发
                form.destroy();//这个导致form.close
              }

            }
          }

        }, 1000);
        form.on("close", () => {
          console.log(" clearInterval(interval_mark);")
          clearInterval(interval_mark);
        });
      }

      form.on("error", (err) => {
        let msg = (err & err.message) ? err.message : util.inspect(err)
        return resolve({
          ok: false,
          msg: `form.on(error)::${msg}`
        })
      })

      form.on("end", () => {
        console.log(Date(), endPoint, _filename, "form.on-end");
        // console.log(" clearInterval(interval_mark);")
        clearInterval(interval_mark);
        setTimeout(_ => {
          if (!upload_has_response) {
            let msg = `${endPoint},${filename},[LATE RESPONSE]no HTTP response after form.end 10s`
            console.log(msg)
            resolve({
              ok: false,
              msg: msg
            })
          }
        }, 10 * 1000)
      });

    })
  }



  /**
   * 
   * @returns {Promise<{ok:Boolean,msg:String,data:{
    * id:String
    * }}>}
    * @param {import("fs").ReadStream|string} stream 
    * @param {String} _filename 
    * @param {String} parent_id 
   * @param {Number} x 
   * @param {Number} define_size 正确的文件大小.如果错误是LATE RESPONSE或者 `type is text/html,status is 504`,就列出一次,找一找符合大小和文件名的文件
   */
  uploadFile__Hunan_XTry(stream, _filename, parent_id = "-11", x = 5, define_size) {
    return new Promise(async resolve => {
      let try_errors = [];
      let count = 0;
      let parsed_filename = path.parse(_filename);
      while (count <= x) {
        let o_try = await this.uploadFile__Hunan(stream, _filename, parent_id);
        if (o_try.ok) {
          return resolve({
            ok: true,
            msg: "ok",
            data: { id: o_try.data.id }
          })
        }
        if (!o_try.msg) {
          console.error("玛德怎么会没有o_try.msg", util.inspect(o_try));
          continue;
        }
        let ERR = o_try.msg.toUpperCase();
        if ((ERR.includes("LATE RESPONSE")
          || ERR.includes("STATUS IS 504")) && define_size >= 5 * 1024 * 1024
          && parsed_filename.name.length >= 5) {
          console.log(_filename, "!!!需要检查一下是否有文件上传成功")
          let o_list = await this.listFiles_old(parent_id, "", 1, 3, "DESC");
          if (o_list.ok) {
            /**
             * @description 搜索条件
             * 1.文件名startWith _filename的前半部分
             * 2.大小一致
             * 3.创建时间在5分钟内 (不过这个不好搞时区 暂时先不写进代码里)
             */
            let filtered = o_list.data.data.filter(e => e.fileSize == define_size && e.fileName.startsWith(parsed_filename.name));
            if (filtered.length) {
              console.log("ERR IS", o_try.msg, "BUT WE FOUND SAME SIZE FILE:");
              for (let o of filtered) {
                console.log(`name : ${o.fileName}
                \nsize : ${o.fileSize}
                \nlastOp : ${o.lastOpTime}
                \n----`)
              }
              return resolve({
                ok: true,
                msg: "ok",
                data: {
                  id: filtered[0].fileId
                }
              })
            }
          }

        }
        if (ERR.includes("SOCKET HANG UP")
          || ERR.includes("ECONNRESET")
          || ERR.includes("ETIMEDOUT")
          || ERR.includes("LATE RESPONSE")) {
          try_errors.push(o_try.msg);
          count++;
        } else {
          return resolve({
            ok: false,
            msg: `error (not hangup or reset):${o_try.msg}`
          })
        }
      }
      return resolve({
        ok: false,
        msg: `X=${x} times fail:${try_errors.join(" , ")}`
      })
    })
  }



  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
    * id:String
    * }}>}
   * @param {String} file_path 
   * @param {String} parent_id 
   */
  uploadFile__Hunan_withSizeCheck(file_path, parent_id = "-11") {
    return new Promise(async resolve => {

      let o_stats = await Toolbox.getStats(file_path);
      if (!o_stats.ok) {
        return resolve({
          ok: false,
          msg: `STATS FAIL:${o_stats.msg}`
        })
      }
      let _filename = path.basename(file_path);
      let stream = fs.createReadStream(file_path);
      let _d = Date.now();
      let o_up = await this.uploadFile__Hunan_XTry(stream, _filename, parent_id, 5, o_stats.stats.size);
      if (!o_up.ok) {
        return resolve({
          ok: false,
          msg: `this.uploadFile__Hunan_XTry FAIL:${o_up.msg}`
        })
      }
      let _d2 = Date.now();
      let o_fileinfo = await this.getFileInfo(o_up.data.id);
      if (!o_fileinfo.ok) {
        return resolve({
          ok: false,
          msg: `FAIL to get fileinfo:${o_fileinfo.msg}`
        })
      }
      if (o_fileinfo.data.fileSize != o_stats.stats.size) {
        let ERROR = `size UN-MATCH:cloud is ${o_fileinfo.data.fileSize},and local disc is ${o_stats.stats.size}`;
        console.error(file_path, ERROR);
        return resolve({
          ok: false,
          msg: ERROR
        })
      }
      {
        let speedByte = o_stats.stats.size / ((_d2 - _d) / 1000);
        console.log(path.basename(file_path), filesize(speedByte), "/s")
      }

      return resolve({
        ok: true,
        msg: "ok",
        data: {
          id: o_up.data.id
        }
      })
    })
  }




  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{
    * id:String
    * }}>}
   * @param {String} file_path 
   * @param {String} parent_id 
   * @param {*Number} x 
   */
  uploadFile__Hunan_withSizeCheck_XTry(file_path, parent_id = "-11", x = 5) {
    return new Promise(async resolve => {
      let errs = [];
      for (let i = 1; i <= x; i++) {
        let o_try = await this.uploadFile__Hunan_withSizeCheck(file_path,
          parent_id);

        if (o_try.ok) {
          return resolve({
            ok: true,
            msg: "ok",
            data: {
              id: o_try.data.id
            }
          })
        } else {
          errs.push(o_try.msg)
        }
      }
      return resolve({
        ok: false,
        msg: `XTry(uploadFile__Hunan_withSizeCheck) fail:${errs.join(" , ")}`
      })
    })
  }

  /**
 * @returns {EventEmitter}
 * @description 如果成功,ev.emit("done",{file_path:String,ok:true,msg:"ok"
 * ,data:{id:String}});
 * 否则 ev.emit("done",{file_path:String,ok:false,msg:String})
 * @param {String} file_path 
 * @param {String} parent_id 
 */
  uploadFile_eventMode(file_path, parent_id = "-11") {
    let ev = new EventEmitter();
    let func = () => {
      let p = this.uploadFile__Hunan_withSizeCheck_XTry(file_path, parent_id);
      p.then(o => {
        if (o.ok) {
          return ev.emit("done", {
            file_path: file_path, ok: true, msg: "ok",
            data: { id: o.data.id }
          })
        } else {
          return ev.emit("done", {
            file_path: file_path, ok: false, msg: o.msg,
          })
        }
      })
      return p;
    }
    this.upload_queue.add(func);
    return ev;
  }


  /**
   * @returns {Promise<{fatal_death:Boolean,errors:string[],data:{folder_id:String}}>}
   * @param {String} dir_path 
   * @param {String} parent_id 
   */
  uploadFolder__Hunan(dir_path, parent_id = "-11") {
    return new Promise(async resolve => {
      let o_ss = await this.initInfoPromise;
      if (!o_ss.ok) {
        return resolve({
          fatal_death: true,
          errors: [`不能获取sessionKey::${o_ss.msg}`]
        })
      }
      let o_createFolder = await this.createFolderUtilIsNew(parent_id,
        path.basename(dir_path));
      if (!o_createFolder.ok) {
        return resolve({
          fatal_death: true,
          errors: [`不能创建第一个文件夹:${o_createFolder.msg}`]
        })
      }
      let o_list = await Toolbox.safeListDir(dir_path);
      /**@type {string[]} */
      let errors = [];
      for (let item of o_list.filter(e => e.stats.isDirectory())) {
        let o_sub = await this.uploadFolder__Hunan(item.full_path,
          o_createFolder.data.fileId);
        errors = errors.concat(o_sub.errors)
      }
      let uploadFileTasks = o_list.filter(e => e.stats.isFile()).map(item => async cb => {
        let o_single = await this.uploadFile__Hunan_withSizeCheck_XTry(item.full_path,
          o_createFolder.data.fileId);
        if (!o_single.ok) {
          errors.push(`上传文件失败:${item.full_path}:${o_single.msg}`)
        }
        cb();
      });
      runParallel(uploadFileTasks, 5, () => {
        return resolve({
          fatal_death: false,
          errors: errors,
          data: {
            folder_id: o_createFolder.data.fileId
          }
        })
      })
    })
  }

  /**
   * @returns {EventEmitter}
   * @description 如果成功,ev.emit("done",{dir_path:String,
   * fatal_death:false,errors:[],
   * data:{folder_id:String}});
   * 否则 ev.emit("done",{dir_path:String,fatal_death:true,errors:[]})
   * @returns {Promise<{fatal_death:Boolean,errors:string[],data:{folder_id:String}}>}
   * @param {String} dir_path 
   * @param {String} parent_id 
   */
  uploadFolder_eventMode(dir_path, parent_id = "-11") {
    let ev = new EventEmitter();
    this.initInfoPromise.then(async o_ss => {
      if (!o_ss.ok) {
        return ev.emit("done", {
          dir_path: dir_path,
          fatal_death: true,
          errors: [`不能获取sessionKey::${o_ss.msg}`]
        });
      }
      let o_createFolder = await this.createFolderUtilIsNew(parent_id,
        path.basename(dir_path));
      if (!o_createFolder.ok) {
        return ev.emit("done", {
          dir_path: dir_path,
          fatal_death: true,
          errors: [`不能创建第一个文件夹:${o_createFolder.msg}`]
        });
      }
      let o_list = await Toolbox.safeListDir(dir_path);
      /**@type {string[]} */
      let errors = [];
      let sub_dirs = o_list.filter(e => e.stats.isDirectory());
      let sub_files = o_list.filter(e => e.stats.isFile());
      let dirs_evs = sub_dirs.map(e => this.uploadFolder_eventMode(e.full_path,
        o_createFolder.data.fileId));
      let files_evs = sub_files.map(e => this.uploadFile_eventMode(e.full_path,
        o_createFolder.data.fileId));
      let done_count = 0;
      let check_if_final = () => {
        done_count++;
        if (done_count == sub_files.length + sub_dirs.length) {
          ev.emit("done", {
            dir_path: dir_path,
            fatal_death: false,
            errors: errors,
            data: {
              folder_id: o_createFolder.data.fileId
            }
          })
        }
      };
      for (let i = 0, n = dirs_evs.length; i < n; i++) {
        let ev = dirs_evs[i];
        ev.on("done", (args) => {
          if (args.fatal_death) {
            let msg = args.errors.join(",");
            msg = `${args.dir_path}::${msg}`;
            errors.push(msg)
          } else {
            errors = errors.concat(args.errors);
          }
          check_if_final();
        })
      }
      for (let i = 0, n = files_evs.length; i < n; i++) {
        let ev = files_evs[i];
        ev.on("done", (args) => {
          if (!args.ok) {
            let msg = `${args.file_path}::${args.msg}`
            errors.push(msg);
          }
          check_if_final();
        })
      }
    })

    return ev;

  }

  /**
   * @returns {Promise<{ok:Boolean,msg:String,data:{folder_id:String}}>}
   * @param {String} file_path 
   * @param {String} parent_id 
   */
  uploadFileAsFolder__Hunan(file_path, parent_id = "-11") {
    return new Promise(async resolve => {
      let o_createFolder = await this.createFolderUtilIsNew(parent_id,
        path.basename(file_path));
      if (!o_createFolder.ok) {
        return resolve({
          ok: false,
          msg: `无法创建文件夹:${o_createFolder.msg}`
        })
      }
      let o_up = await this.uploadFile__Hunan_withSizeCheck_XTry(file_path, o_createFolder.data.fileId);
      if (!o_up.ok) {
        return resolve({
          ok: false,
          msg: `上传过程失败:${o_up.msg}`
        })
      }
      resolve({
        ok: true,
        msg: "ok",
        data: {
          folder_id: o_createFolder.data.fileId
        }
      })
    })
  }

  /**
   * @returns {EventEmitter}
   * @description 如果成功,ev.emit("done",
   * {file_path:String,ok:true,msg:"ok",
   * data:{folder_id:String}
   * })
   * 如果失败,ev.emit("done",{file_path:String,ok:false,msg:String})
   * @param {String} file_path 
   * @param {String} parent_id 
   */
  uploadFileAsFolder_eventMode(file_path, parent_id = "-11") {
    let ev = new EventEmitter();
    this.createFolderUtilIsNew(parent_id, path.basename(file_path)).then(async o_createFolder => {
      if (!o_createFolder.ok) {
        return ev.emit("done", {
          file_path: file_path, ok: false,
          msg: `无法创建文件夹:${o_createFolder.msg}`
        })
      }
      let upload_ev = this.uploadFile_eventMode(file_path, o_createFolder.data.fileId);
      upload_ev.on("done", (args) => {
        if (args.ok) {
          ev.emit("done", {
            file_path: file_path,
            ok: true,
            msg: "ok",
            data: {
              folder_id: o_createFolder.data.fileId
            }
          });
        } else {
          ev.emit("done", {
            file_path: file_path,
            ok: false,
            msg: `上传过程失败::${args.msg}`
          });
        }
      })
    })
    return ev;
  }
}

class ModelData_inListFile {
  /**
   * 
   * @param {String} fileId 
   * @param {String} fileName 
   * @param {Boolean} isFolder 
   * @param {String} parentId 
   * @param {String} createTime just like "2019-10-31 03:56:44"
   * @param {String} lastOpTime  just like "2019-10-31 03:56:44"
   * @param {Number} fileSize file size in bytes ;0 if isFolder
   * @param {String} downloadUrl null string if isFolder
   */
  constructor(fileId, fileName, isFolder, parentId, createTime, lastOpTime, fileSize = 0, downloadUrl = "") {
    this.fileId = fileId;
    this.fileName = fileName;
    this.isFolder = isFolder;
    this.parentId = parentId;
    this.createTime = createTime;
    this.lastOpTime = lastOpTime;
    this.fileSize = fileSize;
    this.downloadUrl = downloadUrl;
  }

  /**
   * @description 返回downloadUrl参数里的fileStr
   */
  get downloadFileAction_FileStr() {
    if (this.isFolder || !this.downloadUrl) {
      return "";
    }
    let parsed = url.parse(this.downloadUrl);
    let queryPart = parsed.query;
    let qs_parsed = querystring.parse(queryPart);
    if (qs_parsed['fileStr']) {
      return qs_parsed['fileStr']
    }
    return ""
  }
}



module.exports = {
  Cloud189Drive
}