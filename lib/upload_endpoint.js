const P2cBalancer = require("load-balancers").P2cBalancer;
const GoodProvinces = [
  // "chongq",
  "anh",
  "anh",
  "chongq",
  "shangh",
  // "fuj",
  // "shanx",
  // "shanx",
  "xinj",
  // "sic",
  // "guizh",
  "yunn",
  "qingh"
  // "hn02"
];
const balancer = new P2cBalancer(GoodProvinces.length);


function getUploadEndpoint() {
  let wechoose = GoodProvinces[balancer.pick()];
  return `https://${wechoose}.upload.cloud.189.cn/v1/DCIWebUploadAction`;
}


module.exports = {
  getUploadEndpoint
}