const axios = require("axios").default;
const WEBHOOK_URL = "https://hook.worktile.com/incoming/b71f1255a1bb47449592537b0b067e77";

/**
 * 
 * @param {String} title 
 * @param {String} content 
 */
function log(title, content) {
  return new Promise(resolve => {
    axios.post(WEBHOOK_URL, {
      fallback: content,
      color: "#FF0080",  //将消息的正文用指定的颜色进行标示
      pretext: content.substr(0, 100),
      author_name: "DEBUG 189",  //用于显示作者名
      author_icon: "http://m.tuniucdn.com/fb2/t1/G4/M00/94/FB/Cii_J10YKNOIHtIyAABxLdaKDFsAAHgLAFfvDcAAHFF549.png",  //作者
      title: title,
      text: `${title}\n
      
      \n${content}`
    }).then(axresp => {
      resolve()
    }).catch(e => resolve())
  })
}


module.exports = {
  log
}