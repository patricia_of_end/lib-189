const util = require("util");

const CommonAxerrHandlerGen = (resolve) => (axerr) => {
    if (axerr.response) {
        resolve({
            ok: false,
            msg: `HTTP ${axerr.response.status} ${axerr.response.statusText} : ${axerr.response.data ?
                (axerr.response.data['message'] ? axerr.response.data['message'] : util.inspect(axerr.response.data)) :
                "!NO HTTP RESPONSE DATA"}`
        })
    } else {
        resolve({
            ok: false,
            msg: axerr.message ? axerr.message : util.inspect(axerr)
        });
    }
};


module.exports = CommonAxerrHandlerGen