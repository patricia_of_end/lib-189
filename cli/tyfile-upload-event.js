#!/usr/bin/env node
const getUploadPaths = require("./get_upload_paths").getUploadPaths;
const args = require("args");
const process = require("process");
let cwd = process.cwd();
const path = require("path");
const runParallel = require("run-parallel-limit");
console.log("cwd :", cwd);
let flags = args.parse(process.argv);
let sub = args.sub;
const Toolbox = require("../tool/toolbox");
const err_log_hook = require("./cli_err_webhook");
const record_hook = require("./to_upload_daily_webhook");
const E189 = require("../lib/E189").E189;
const fs = require("fs-extra");

const PHONE = "15019447005";
const PASS = "tianyi65*huji";
const ACCOUNT_CODE = "7005"//"6990"

let e189 = new E189(PHONE, PASS);

const ROOT_DIR = "7139433325144275"
e189.get_sson_promise.then(async o_sson => {
  if (!o_sson.ok) {
    err_log_hook.logError("登陆|SSON失败", o_sson.msg);
    console.error("登陆|SSON失败", o_sson.msg)
    return process.exit(1);
  }
  let o_c189 = await o_sson.data.sson.getNewCloud189Drive();
  if (!o_c189.ok) {
    err_log_hook.logError("登陆|getNewCloud189Drive失败", o_c189.msg)
    console.error("登陆|getNewCloud189Drive失败", o_c189.msg)
    return process.exit(1);
  }
  let c189 = o_c189.data.c189;
  let avai_paths = await getUploadPaths(cwd, sub);
  let dir_aps = avai_paths.filter(e => e.stats.isDirectory());
  let file_aps = avai_paths.filter(e => e.stats.isFile());
  let file_evs = file_aps.map(e => c189.uploadFileAsFolder_eventMode(e.full_path,
    ROOT_DIR));
  let dir_evs = dir_aps.map(e => c189.uploadFolder_eventMode(e.full_path, ROOT_DIR));
  let done_count = 0;
  let check_alldone = () => {
    done_count++;
    if (done_count == avai_paths.length) {
      console.log("all done");
      process.exit();
    }
  };
  file_evs.forEach(ev => {
    ev.on("done", async (args) => {
      if (args.ok) {
        success_content = `
        \n天翼上传 ${ACCOUNT_CODE}
        \n${markdownify(args.file_path)}
        \n\`\`\`
        \n 公开URL : http://ty-file.liblu.me/ps/drive/a/${ACCOUNT_CODE}/d/${args.data.folder_id}/p/1
        \n\`\`\`
        \n 自己用 : http://ty-file.liblu.me/drive/a/${ACCOUNT_CODE}/d/${args.data.folder_id}/p/1
        \n
        `
        await createTextFile(path.join(cwd, `${path.basename(args.file_path)}.OKTY_html`), success_content)
      } else {
        await createTextFile(path.join(cwd, `${path.basename(args.file_path)}.TY-FAIL`), `${args.file_path}::\n${args.msg}`)
      }
      check_alldone();
    })
  });
  dir_evs.forEach(ev => {
    ev.on("done", async args => {
      if (args.fatal_death) {
        await createTextFile(path.join(cwd, `${path.basename(args.dir_path)}.TY-FAIL`),
          `${args.dir_path}::致命失败\n${args.errors.join("\n")}`)
      } else {
        if (args.errors.length) {
          await createTextFile(path.join(cwd, `${path.basename(args.dir_path)}.OKTY_html-PART-FAIL`),
            args.errors.join("\n ---- \n"));
        }
        success_content = `
      \n天翼上传 ${ACCOUNT_CODE}
      \n${markdownify(args.dir_path)}
      \n
      \n---
      \n
      \n\`\`\`
      \n 公开URL : http://ty-file.liblu.me/ps/drive/a/${ACCOUNT_CODE}/d/${args.data.folder_id}/p/1
      \n\`\`\`
      \n 自己用 : http://ty-file.liblu.me/drive/a/${ACCOUNT_CODE}/d/${args.data.folder_id}/p/1
      \n
      \n ---
      \n ### errors (if error) ?
      \n ${args.errors.map(e => `\`${e}\``).join("\n")}`;
        await createTextFile(path.join(cwd, `${path.basename(args.dir_path)}.OKTY_html`), success_content);

      }
      check_alldone();
    })
  })
});



/**
 * @description 转义一般文本中的 []()\等特殊符号
 * @param {String} str 
 */
function markdownify(str) {
  str = str.replace(/\\/g, "\\\\");
  str = str.replace(/\[/g, "\\[");
  str = str.replace(/\]/g, "\\]");
  str = str.replace(/\(/g, "\\(");
  str = str.replace(/\)/g, "\\)");
  return str;
}


/**
 * @returns {Promise<{ok:Boolean,msg:String}>}
 * @param {String} file_fullpath 
 * @param {String} content 
 */
function createTextFile(file_fullpath, content) {
  return new Promise(async resolve => {
    fs.writeFile(file_fullpath, content, {
      flag: "w+",
      encoding: "utf8"
    }, (err) => {
      if (err) {
        return resolve({
          ok: false,
          msg: `${err.message}`
        })
      }
      resolve({
        ok: true,
        msg: "ok"
      })
    })
  })
}