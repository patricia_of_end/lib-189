const axios = require("axios").default;
const CommonAxerrH = require("../tool/CommonAxerrHandler");
const util = require("util");
const WEBHOOK_URL = "https://hook.worktile.com/incoming/de9ba623a8b94266a8d38609424231a7";


/**
 * 
 * @param {String} subject 
 * @param {String} info_html 
 */
function logError(subject, info_html) {
  return new Promise(r => {
    axios.post(WEBHOOK_URL, {
      attachment: {
        fallback: `${subject}:${info_html.slice(0, 50)}`,
        title: subject,
        text: info_html,
        color: "#FF0000",
        pretext: ``,
        fields: []
      }
    }, {
      headers: {
        "Content-Type": "application/json"
      }
    }).then(_ => r()).catch(axerr => {
      console.log("!!! 网络错误 未能成功LOG记录错误 !!!");
      console.log("[SUBJECT] : ", subject);
      console.log(info_html);
      console.log("\n", "网络问题是=", util.inspect(axerr));
      console.log("\n----------========-----------\n");
      r();
    })
  })

}


module.exports = {
  logError
}

