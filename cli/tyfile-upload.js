#!/usr/bin/env node
const getUploadPaths = require("./get_upload_paths").getUploadPaths;
const args = require("args");
const process = require("process");
let cwd = process.cwd();
const path = require("path");
const runParallel = require("run-parallel-limit");
console.log("cwd :", cwd);
let flags = args.parse(process.argv);
let sub = args.sub;
const Toolbox = require("../tool/toolbox");
const err_log_hook = require("./cli_err_webhook");
const record_hook = require("./to_upload_daily_webhook");
const E189 = require("../lib/E189").E189;
const fs = require("fs-extra");

const PHONE = '15343233350'//"15019447005";
const PASS = 'ty65*huji'//"tianyi65*huji";
const ACCOUNT_CODE = "3350"

let e189 = new E189(PHONE, PASS);

const ROOT_DIR = '6130138710103472';//"7139433325144275"
e189.get_sson_promise.then(async o_sson => {
  if (!o_sson.ok) {
    err_log_hook.logError("登陆|SSON失败", o_sson.msg);
    console.error("登陆|SSON失败", o_sson.msg)
    return process.exit(1);
  }
  let o_c189 = await o_sson.data.sson.getNewCloud189Drive();
  if (!o_c189.ok) {
    err_log_hook.logError("登陆|getNewCloud189Drive失败", o_c189.msg)
    console.error("登陆|getNewCloud189Drive失败", o_c189.msg)
    return process.exit(1);
  }
  let c189 = o_c189.data.c189;
  let avai_paths = await getUploadPaths(cwd, sub);
  let tasks = avai_paths.map(ap => async cb => {
    let success_content = "";
    if (ap.stats.isFile()) {
      let o_up = await c189.uploadFileAsFolder__Hunan(ap.full_path, ROOT_DIR);
      if (!o_up.ok) {
        await err_log_hook.logError("上传失败", `
        \n ${markdownify(ap.full_path)}
        \n ERR = 
        \n\`\`\`
        ${o_up.msg}
        \n\`\`\`
        `);
        await createTextFile(path.join(cwd, `${path.basename(ap.full_path)}.TY-FAIL`), o_up.msg);
        return cb();
      }
      success_content = `
      \n天翼上传 ${ACCOUNT_CODE}
      \n${markdownify(ap.full_path)}
      \n
      \n---
      \n
      \n\`\`\`
      \n 公开URL : http://ty-file.liblu.me/ps/drive/a/${ACCOUNT_CODE}/d/${o_up.data.folder_id}/p/1
      \n\`\`\`
      \n 自己用 : http://ty-file.liblu.me/drive/a/${ACCOUNT_CODE}/d/${o_up.data.folder_id}/p/1
      \n
      `
    } else if (ap.stats.isDirectory()) {
      let o_up = await c189.uploadFolder__Hunan(ap.full_path, ROOT_DIR);
      if (o_up.fatal_death) {
        await err_log_hook.logError("上传致命的失败", `
        \n ${markdownify(ap.full_path)}
        \n ERR = 
        \n\`\`\`
        ${o_up.errors.join("\n")}
        \n\`\`\`
        `);
        await createTextFile(path.join(cwd, `${path.basename(ap.full_path)}.TY-FAIL`), o_up.msg);
        return cb()
      }
      if (o_up.errors.length) {
        await err_log_hook.logError("上传__部分失败", `
        \n ${markdownify(ap.full_path)}
        \n ERR = 
        \n\`\`\`
        ${o_up.errors.join("\n")}
        \n\`\`\`
        `);
        await createTextFile(path.join(cwd, `${path.basename(ap.full_path)}.OKTY_html-PART-FAIL`),
          o_up.errors.join("\n ---- \n"));
      }

      success_content = `
      \n天翼上传 ${ACCOUNT_CODE}
      \n${markdownify(ap.full_path)}
      \n
      \n---
      \n
      \n\`\`\`
      \n 公开URL : http://ty-file.liblu.me/ps/drive/a/${ACCOUNT_CODE}/d/${o_up.data.folder_id}/p/1
      \n\`\`\`
      \n 自己用 : http://ty-file.liblu.me/drive/a/${ACCOUNT_CODE}/d/${o_up.data.folder_id}/p/1
      \n
      \n ---
      \n ### errors 
      \n ${o_up.errors.map(e => `\`${e}\``).join("\n")}
      `
    }

    // let toDailyUpload = await record_hook.createTask(path.basename(ap.full_path),
    //   success_content);
    // if (!toDailyUpload.ok) {
    //   await createTextFile(path.join(cwd, `${path.basename(ap.full_path)}.webhook-fail`), toDailyUpload.msg);
    // }
    await createTextFile(path.join(cwd, `${path.basename(ap.full_path)}.OKTY_html`), success_content);

    cb();
  });
  runParallel(tasks, 1, () => {
    console.log("parallel done");
    process.exit(0);
  })
});



/**
 * @description 转义一般文本中的 []()\等特殊符号
 * @param {String} str 
 */
function markdownify(str) {
  str = str.replace(/\\/g, "\\\\");
  str = str.replace(/\[/g, "\\[");
  str = str.replace(/\]/g, "\\]");
  str = str.replace(/\(/g, "\\(");
  str = str.replace(/\)/g, "\\)");
  return str;
}


/**
 * @returns {Promise<{ok:Boolean,msg:String}>}
 * @param {String} file_fullpath 
 * @param {String} content 
 */
function createTextFile(file_fullpath, content) {
  return new Promise(async resolve => {
    fs.writeFile(file_fullpath, content, {
      flag: "w+",
      encoding: "utf8"
    }, (err) => {
      if (err) {
        return resolve({
          ok: false,
          msg: `${err.message}`
        })
      }
      resolve({
        ok: true,
        msg: "ok"
      })
    })
  })
}